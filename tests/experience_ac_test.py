import os
import unittest
from pathlib import Path

import h5py
import numpy as np
import tensorflow as tf

from dlgo.encoders.base import get_encoder_by_name
from dlgo.exp.exp_reader import ExpReader
from dlgo.exp.exp_writer import ExpWriter
from dlgo.tools.board_decoder import BoardDecoder
from init_ac_agent import Initiator
from scripts.simulate import Dispatcher

keras = tf.keras
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'


class EpisodeExperienceCollectorTest(unittest.TestCase):
    def setUp(self):
        self.board_size = 9
        self.encoder = get_encoder_by_name('simple', self.board_size)
        self.num_planes = self.encoder.num_planes
        self.num_games = 2
        self.project_path = Path.cwd()
        self.exp_path = self.project_path / 'exp' / f'exp_{self.num_games}_experience_test.h5'
        self.model_name = 'model_experience_test.h5'
        self.model_full_path = self.project_path / 'models' / self.model_name
        self.collector = ExpWriter(str(self.exp_path), self.board_size, self.num_planes)
        print(f'PROJECT PATH: {self.project_path}')

    def tearDown(self):
        pass
        # self.clean_up()

    def clean_up(self):
        # self.unlink_file(self.model_full_path)
        self.unlink_file(self.exp_path)

    @staticmethod
    def unlink_file(file):
        if Path(file).is_file():
            Path.unlink(file)

    def test_begin_episode(self):
        self.collector.begin_episode()
        self.assertEqual(len(self.collector._current_episode_states), 0)
        self.assertEqual(len(self.collector._current_episode_actions), 0)
        self.assertEqual(len(self.collector._current_episode_estimated_values), 0)

    def test_record_decision(self):
        self.clean_up()
        state = np.zeros((self.board_size, self.board_size, self.num_planes))
        state[3, 8, 0] = 1
        action = 1
        estimated_value = 1.0
        self.collector.record_decision(state, action, estimated_value)
        self.assertEqual(len(self.collector._current_episode_states), 1)
        self.assertEqual(len(self.collector._current_episode_actions), 1)
        self.assertEqual(len(self.collector._current_episode_estimated_values), 1)

    def test_complete_episode(self):
        state = np.zeros((self.board_size, self.board_size, self.num_planes))
        state[3, 8, 0] = 1
        action = 2
        reward = 1
        self.collector.begin_episode()
        self.collector.record_decision(state, action)
        self.collector.complete_episode(reward)
        with h5py.File(self.exp_path, 'r') as f:
            self.assertIn('experience', f.keys())
            self.assertIn('states', f['experience'].keys())
            self.assertIn('actions', f['experience'].keys())
            self.assertIn('rewards', f['experience'].keys())
            self.assertIn('advantages', f['experience'].keys())

    def test_length(self):
        gen = ExpReader(str(self.exp_path), 1, self.num_planes, self.board_size)
        length = gen.num_states()
        print(f'Length: {length}')
        self.assertEqual(length, 1)

    def test_two_ways_of_counting_states(self):
        with h5py.File(self.exp_path, 'r') as f:
            # two equivalent methods for acquiring the volume of the experience file
            num_states = len(f['experience/states'])
            num_states2 = f['experience']['states'].shape[0]
            self.assertEqual(num_states, num_states2, f'{num_states} and {num_states2}')

    def test_written_shapes_ac(self):
        batch_size = 1
        gen = ExpReader(str(self.exp_path), batch_size, self.num_planes, self.board_size, client='ac')
        length = gen.num_states()
        print(f'The length of the experience file (the number of states/positions): {length}\n')
        next_batch = gen.generate()

        for i, (states, targets) in enumerate(next_batch):
            if i >= length // batch_size:
                break
            msg = (
                f'Batch {i}: states shape={states.shape}\npolicy target shape={targets[0].shape}\n'
                f'value target shape={targets[1].shape}'
            )
            print(msg)
            self.assertEqual(states.shape, (1, self.board_size, self.board_size, self.num_planes))
            self.assertEqual(targets[0].shape, (1, self.board_size * self.board_size))
            self.assertEqual(targets[1].shape, (1,))

    def test_visualize_experience_ac(self):
        print(f'Collector length: {len(self.collector)}')

        print(f'*' * 40)
        print(f'THE CONTENT OF THE EXP FILE:')
        print(f'*' * 40)

        batch_size = 1
        gen = ExpReader(str(self.exp_path), batch_size, self.num_planes, self.board_size, seed=1234, client='ac')
        length = gen.num_states()
        print(f'LENGTH: {length}')
        next_batch = gen.generate()

        for i, (states, targets) in enumerate(next_batch):
            if i >= length // batch_size:
                break
            for item in range(batch_size):
                print(f'ITEM: {item}')
                decoder = BoardDecoder(states[item])
                decoder.print()
                print(f'')
                move = np.argmax(targets[0][item], axis=None, out=None)
                point = self.encoder.decode_point_index(move)
                # print(move)
                print(point)
                print(f'')
                print(f'POLICY TARGET (THE ACTOR):')
                print(f'{targets[0][item]}')
                print(f'VALUE TARGET (THE CRITIC):')
                print(f'{targets[1][item]}')

    def test_h5_structure(self):
        """
        Recursively prints the HDF5 file structure.
        """
        self.print_h5_structure(h5py.File(self.exp_path), 0)

    def print_h5_structure(self, obj, indent=0):
        indent_str = "  " * indent
        if isinstance(obj, h5py.File):
            print(indent_str + "<File>")
            for key in obj.keys():
                self.print_h5_structure(obj[key], indent=indent + 1)
        elif isinstance(obj, h5py.Dataset):
            print(indent_str + "<Dataset: {} (dtype: {})>".format(obj.name, obj.dtype))
        elif isinstance(obj, h5py.Group):
            print(indent_str + "<Group: {}>".format(obj.name))
            for key in obj.keys():
                self.print_h5_structure(obj[key], indent=indent + 1)


if __name__ == '__main__':
    unittest.main()
