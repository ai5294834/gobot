import unittest
import os
import sys

from dlgo.agent import termination
from dlgo.agent import loader
from dlgo.gtp.frontend import GTPFrontend
from dlgo.gtp.command import Command


class EpisodeExperienceCollectorTest(unittest.TestCase):
    def setUp(self):
        self.agent = self.create_bot()
        self.frontend = GTPFrontend(self.agent)

    def create_bot(self):
        agent_loader = loader.Loader("mcts")
        agent = agent_loader.create_bot()
        strategy = termination.get("opponent_passes")
        return termination.TerminationAgent(agent, strategy)

    def test_list_commands(self):
        print(self.frontend.handle_list_commands())

