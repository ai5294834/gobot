import os
import unittest
from pathlib import Path

import h5py
import numpy as np
import tensorflow as tf

from dlgo.encoders.base import get_encoder_by_name
from dlgo.exp.exp_reader import ExpReader
from dlgo.exp.exp_writer import ExpWriter
from dlgo.tools.board_decoder import BoardDecoder

keras = tf.keras
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'


class EpisodeExperienceCollectorTest(unittest.TestCase):
    def setUp(self):
        self.board_size = 9
        self.encoder = get_encoder_by_name('simple', self.board_size)
        self.num_planes = self.encoder.num_planes
        self.num_games = 2
        self.project_path = Path.cwd()
        self.exp_path = self.project_path / 'exp' / f'exp_{self.num_games}_experience_test.h5'
        self.model_name = 'model_experience_test.h5'
        self.model_full_path = self.project_path / 'models' / self.model_name
        self.collector = ExpWriter(str(self.exp_path), self.board_size, self.num_planes)
        print(f'PROJECT PATH: {self.project_path}')

    def tearDown(self):
        pass
        # self.clean_up()

    def clean_up(self):
        self.exp_path = self.project_path / 'exp' / f'exp_{self.num_games}_experience_test.h5'
        self.unlink_file(self.exp_path)

    @staticmethod
    def unlink_file(file):
        if Path(file).is_file():
            Path.unlink(file)

    def test_begin_episode(self):
        self.collector.begin_episode()
        self.assertEqual(len(self.collector._current_episode_states), 0)
        self.assertEqual(len(self.collector._current_episode_actions), 0)
        self.assertEqual(len(self.collector._current_episode_estimated_values), 0)

    def test_record_decision(self):
        state = np.zeros((self.board_size, self.board_size, self.num_planes))
        state[3, 8, 0] = 1
        action = 1
        estimated_value = 1.0
        self.collector.record_decision(state, action, estimated_value)
        self.assertEqual(len(self.collector._current_episode_states), 1)
        self.assertEqual(len(self.collector._current_episode_actions), 1)
        self.assertEqual(len(self.collector._current_episode_estimated_values), 1)

    def test_complete_episode(self):
        self.clean_up()
        self.board_size = 9
        state = np.zeros((self.board_size, self.board_size, self.num_planes))
        state[3, 8, 0] = 1
        action = 2
        reward = 1
        self.collector.begin_episode()
        self.collector.record_decision(state, action)
        self.collector.complete_episode(reward)
        with h5py.File(self.exp_path, 'r') as f:
            self.assertIn('experience', f.keys())
            self.assertIn('states', f['experience'].keys())
            self.assertIn('actions', f['experience'].keys())
            self.assertIn('rewards', f['experience'].keys())
            self.assertIn('advantages', f['experience'].keys())

    def test_length(self):
        self.exp_path = Path('/Users/marcin/PycharmProjects/gobot/exp/exp2_model_sl_strong_improved3_20000_1_epoch1_24proc.h5')
        self.board_size = 19
        with h5py.File(self.exp_path, 'r') as f:
            num_states = f['experience']['states'].shape[0]
        gen = ExpReader(str(self.exp_path), 1, self.num_planes, self.board_size)
        length = gen.num_states()
        print(f'Length: {length}')
        self.assertEqual(length, num_states)

    def test_two_ways_of_counting_states(self):
        with h5py.File(self.exp_path, 'r') as f:
            # two equivalent methods for acquiring the volume of the experience file
            num_states = len(f['experience/states'])
            num_states2 = f['experience']['states'].shape[0]
            self.assertEqual(num_states, num_states2, f'{num_states} and {num_states2}')

    def test_written_shapes_pg(self):
        self.exp_path = Path(
            '/Users/marcin/PycharmProjects/gobot/exp/exp1_model_sl_strong_improved3_20000_1_epoch1_24proc.h5')
        self.board_size = 19
        batch_size = 256
        # batch_size = 1
        gen = ExpReader(str(self.exp_path), batch_size, self.num_planes, self.board_size, client='pg')
        length = gen.num_states()
        print(f'The length of the experience file (the number of states/positions): {length}\n')
        next_batch = gen.generate()

        for i, (states, targets) in enumerate(next_batch):
            if i >= length // batch_size:
                break
            msg = (
                f'Batch {i}: states shape={states.shape}\npg policy target shape={targets.shape}\n'
            )
            print(msg)
            self.assertEqual(states.shape, (batch_size, self.board_size, self.board_size, self.num_planes))
            self.assertEqual(targets.shape, (batch_size, self.board_size * self.board_size))

    def test_visualize_experience_pg(self):
        print(f'Collector length: {len(self.collector)}')

        print(f'*' * 40)
        print(f'THE CONTENT OF THE EXP FILE:')
        print(f'*' * 40)

        batch_size = 1
        gen = ExpReader(str(self.exp_path), batch_size, self.num_planes, self.board_size, seed=1234, client='pg')
        length = gen.num_states()
        print(f'LENGTH: {length}')
        next_batch = gen.generate()

        for i, (states, targets) in enumerate(next_batch):
            if i >= length // batch_size:
                break
            for item in range(batch_size):
                print(f'ITEM: {item}')
                decoder = BoardDecoder(states[item])
                decoder.print()
                print(f'')
                move = np.argmax(targets[item], axis=None, out=None)
                if move == 0:
                    move = np.argmin(targets[item], axis=None, out=None)
                point = self.encoder.decode_point_index(move)
                print(point)
                print(f'')
                print(f'POLICY TARGET:')
                print(f'{targets[item]}')

    def test_visualize_random_position_in_experience_pg(self):
        self.exp_path = Path(
            '/Users/marcin/PycharmProjects/gobot/exp/exp1_model_sl_strong_improved3_20000_1_epoch1_24proc.h5')
        self.board_size = 19
        batch_size = 256
        # batch_size = 1
        print(f'Collector length: {len(self.collector)}')

        print(f'*' * 40)
        print(f'THE CONTENT OF THE EXP FILE:')
        print(f'*' * 40)

        gen = ExpReader(str(self.exp_path), batch_size, self.num_planes, self.board_size, seed=1234, client='pg')
        length = gen.num_states()
        print(f'LENGTH: {length}')
        next_batch = gen.generate()

        for i, (states, targets) in enumerate(next_batch):
            if i == 256:
                for item in range(5):
                    print(f'ITEM: {item}')
                    # print(f'STATES SHAPE: {states.shape}')
                    decoder = BoardDecoder(states[item])
                    decoder.print()
                    print(f'')
                    print(f'TARGETS:')
                    print(f'{targets[item]}')
                    move = np.argmax(targets[item], axis=None, out=None)
                    if move == 0:
                        move = np.argmin(targets[item], axis=None, out=None)
                    print(f'move: {move}')
                    point = self.encoder.decode_point_index(move)
                    print(point)
                    # print(f'')
                    # print(f'POLICY TARGET:')
                    # print(f'{targets[item]}')
                break

    def test_board_decoder(self):
        self.exp_path = Path(
            '/Users/marcin/PycharmProjects/gobot/exp/exp1_model_sl_strong_improved3_20000_1_epoch1_24proc.h5')
        self.board_size = 19
        batch_size = 256
        # batch_size = 1

        gen = ExpReader(str(self.exp_path), batch_size, self.num_planes, self.board_size, seed=1234, client='pg')
        next_batch = gen.generate()

        for i, (states, targets) in enumerate(next_batch):
            if i == 256:
                decoder1 = BoardDecoder(states[0])
                decoder1.print()
                print(f'')
                decoder2 = BoardDecoder(states[1])
                decoder2.print()
                print(f'')
                decoder1.compare(decoder2)
                break

    def test_h5_structure(self):
        """
        Recursively prints the HDF5 file structure.
        """
        # self.exp_path = Path('/Users/marcin/PycharmProjects/gobot/exp/exp1_model_sl_strong_improved3_20000_1_epoch1_24proc.h5')
        self.print_h5_structure(h5py.File(self.exp_path), 0)

    def print_h5_structure(self, obj, indent=0):
        indent_str = "  " * indent
        if isinstance(obj, h5py.File):
            print(indent_str + "<File>")
            for key in obj.keys():
                self.print_h5_structure(obj[key], indent=indent + 1)
        elif isinstance(obj, h5py.Dataset):
            print(indent_str + "<Dataset: {} (dtype: {})>".format(obj.name, obj.dtype))
        elif isinstance(obj, h5py.Group):
            print(indent_str + "<Group: {}>".format(obj.name))
            for key in obj.keys():
                self.print_h5_structure(obj[key], indent=indent + 1)


if __name__ == '__main__':
    unittest.main()
