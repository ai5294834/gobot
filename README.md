# gobot

## Name
The goal of the project was to create an AI bot that can play the game of go - hence the name of the project: 'gobot'.

## Description
The project was done purely for educational reasons. The idea was to learn about deep learning while creating something that actually works (and plays).

## Code
The code is heavily inspired by the book 'Deep Learning and the Game of Go' by Max Pumperla and Kevin Ferguson. I rewrote it whenever it suited my goals or when I tried to understand it deeper. I believe that modifying someone's code is the best way to understand how it works. Below, in the next paragraph, there is a short resume of what exactly is borrowed from the book and what is mine. I did this distinction as honestly as I could, granted that after a few months of working on the project, reading and re-reading the code, embellishing it and changing, I may treat it as my own even if it's not the case.

Anyway, the differences and similarities can be easily verified by the readers themselves if they only want to compare side-by-side my repo and the original one which is here: https://github.com/maxpumperla/deep_learning_and_the_game_of_go 

## What (I think) is mine
- network architecture
- the classes responsible for writing and reading RL experience (dlgo/exp folder) 
- some part of the data_processor.py as I looked for a way to make the code easier (to myself)
- many changes in files located in the 'scripts' folder as I tried to bend the code to my needs
- some adjustments in the simple encoder
- some changes in agents, especially when it came to reading RL experience
- minor adjustments in gtp frontend
- some tools (board_decoder.py)
- tests
- Dockerfile

## Installation
There is no need to install anything. The end product of the project is accessible at the OGS server (https://online-go.com) as 'alalupo_bot' ('alalupo' is my nickname). You only need to have an account there. Log in, search for the name of the bot, visit its profile, click on its name and send it a challenge to start playing.

## Acknowledgments
Big thanks to Max Pumperla and Kevin Ferguson for their idea of writing a book that is at the same time attractive to any go lover as well as anyone who wants to get to know the basics of deep learning.

## License
Not applicable here.

## Project status
I treat the project as open. I think I can still create a stronger bot if I only put more time and hardware in the process of teaching the model.
