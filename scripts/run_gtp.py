#!/usr/bin/env python
import os
import sys
os.environ["CUDA_VISIBLE_DEVICES"] = ""
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'


this_directory = os.path.dirname(__file__)
project_directory = os.path.dirname(this_directory)
dlgo_directory = os.path.join(project_directory, 'dlgo')
sys.path.append(project_directory)
sys.path.append(this_directory)
sys.path.append(dlgo_directory)

from dlgo.gtp import GTPFrontend
from dlgo.agent import termination
from dlgo.agent import loader


def main():
    agent_loader = loader.Loader("mcts")
    agent = agent_loader.create_bot()
    strategy = termination.get("opponent_passes")
    termination_agent = termination.TerminationAgent(agent, strategy)
    frontend = GTPFrontend(termination_agent)
    frontend.run()


if __name__ == "__main__":
    main()
