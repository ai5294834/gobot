# Stage 1: Build Python environment
FROM python:3.10 AS python-build

# Set working directory in the container
WORKDIR /app

# Install virtualenv
RUN pip install virtualenv

# Create and activate a virtual environment
RUN virtualenv venv
ENV PATH="/app/venv/bin:$PATH"

# Copy and install Python dependencies
COPY requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt

# Stage 2: Use NVM to install Node.js version 18
FROM python:3.10-slim AS nodejs-build

# Install system dependencies for NVM and Node.js
RUN apt-get update && apt-get install -y curl build-essential

# Install NVM (Node Version Manager)
ENV NVM_DIR /root/.nvm
RUN curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.38.0/install.sh | bash
RUN . $NVM_DIR/nvm.sh && nvm install 18.17.0


# Set working directory in the container
WORKDIR /app

# Create a fresh package.json file with all necessary dependencies
RUN echo '{ "dependencies": { "gtp2ogs": "^8.0.3", "forever": "^0.10.11", "optimist": "^0.5.2", "socket.io-client": "^4.7.1", "tracer": "=0.8.7" } }' > package.json

# Install Node.js dependencies including gtp2ogs
RUN . $NVM_DIR/nvm.sh && npm install

# Copy the gtp2ogs_config.json file from the main directory
COPY gtp2ogs_config.json /app/gtp2ogs_config.json

# Stage 3: Final image with both Python and Node.js
FROM python:3.10-slim

# Set working directory to the app folder
WORKDIR /app

# Install necessary system dependencies for your Python and Node.js programs
RUN apt-get update && apt-get install -y build-essential

# Copy Python dependencies from the first stage
COPY --from=python-build /app/venv /app/venv

# Set the virtual environment in the PATH
ENV PATH="/app/venv/bin:$PATH"

# Copy Node.js dependencies from the second stage
COPY --from=nodejs-build /app/node_modules /app/node_modules

# Copy the Node.js executable from the second stage
COPY --from=nodejs-build /root/.nvm/versions/node/v18.17.0/bin/node /usr/local/bin/

# Copy your entire project code
COPY . /app

# Set execute permissions for the 'run_gtp.py' script
RUN chmod +x /app/scripts/run_gtp.py

# Run your GTP2OGS program with the relative path to the config file
CMD ["node_modules/.bin/gtp2ogs", "--config", "gtp2ogs_config.json"]
